package com.vnacks.vnacks.asyncs;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.vnacks.vnacks.adapters.VideosRecyclerViewAdapter;
import com.vnacks.vnacks.database.DatabaseHelper;
import com.vnacks.vnacks.models.Video;
import com.vnacks.vnacks.models.VideoFilter;
import com.vnacks.vnacks.views.VideosFragment;

import java.util.EnumSet;

/**
 * Created by seung on 6/6/2016.
 */
public class SetWatchedVideoAsync extends AsyncTask<Void, Void, Void> {

    private Context context;
    private VideosRecyclerViewAdapter adapter;
    private Video video;
    private EnumSet<VideoFilter> videoFilters;

    public SetWatchedVideoAsync(Context context, VideosRecyclerViewAdapter adapter, Video video, EnumSet<VideoFilter> videoFilters) {
        this.context = context;
        this.adapter = adapter;
        this.video = video;
        this.videoFilters = videoFilters;
    }

    @Override
    protected Void doInBackground(Void... params) {
        SQLiteDatabase db = new DatabaseHelper(this.context).getReadableDatabase();
        Cursor cursor = db.rawQuery(String.format("SELECT id FROM watched_videos WHERE id = '%s'", this.video.id), null);
        if (cursor.getCount() <= 0) {
            db.execSQL(String.format("INSERT INTO watched_videos VALUES ('%s', datetime())", this.video.id));
        }

        cursor.close();
        return null;
    }

    @Override
    protected void onPostExecute(Void arg) {
        this.video.watched = true;
        if (!this.videoFilters.contains(VideoFilter.WATCHED)) {
            this.adapter.getVideos().remove(this.video);
        }

        this.adapter.notifyDataSetChanged();
    }
}

