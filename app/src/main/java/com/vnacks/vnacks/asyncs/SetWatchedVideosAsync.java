package com.vnacks.vnacks.asyncs;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.vnacks.vnacks.database.DatabaseHelper;
import com.vnacks.vnacks.models.Video;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by laraib on 11/22/2017.
 */

public class SetWatchedVideosAsync extends AsyncTask<Void, Void, Void> {

    private Context context;
    private Video video;

    public SetWatchedVideosAsync(Context context, Video video) {
        this.context = context;
        this.video = video;
    }

    @Override
    protected Void doInBackground(Void... params) {
        SQLiteDatabase db = new DatabaseHelper(this.context).getWritableDatabase();
        ContentValues values = new ContentValues();
        StringBuilder responseStrBuilder = new StringBuilder();
        JSONArray campaignsJson = null;
        JSONObject videoJson = null;
        try {
            campaignsJson = new JSONArray(responseStrBuilder.toString());
            for (int i = 0; i < campaignsJson.length(); i++)
                videoJson = campaignsJson.getJSONObject(i);
            values.put("url", videoJson.getString("link"));
            values.put("isWatched", 1);
            db.insert("videos", null, values);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        db.close();

        return null;
    }

    @Override
    protected void onPostExecute(Void arg) {

    }

}
