package com.vnacks.vnacks.asyncs;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.vnacks.vnacks.adapters.VideosRecyclerViewAdapter;
import com.vnacks.vnacks.database.DatabaseHelper;
import com.vnacks.vnacks.models.Video;
import com.vnacks.vnacks.models.VideoFilter;

import java.util.EnumSet;
import java.util.List;

/**
 * Created by laraib on 11/26/2017.
 */

public class GetFavVideosAync extends AsyncTask<String, Void, Void> {

    private Context context;
    private Video video;
    private EnumSet<VideoFilter> videoFilters;
    private String url;
    private VideosRecyclerViewAdapter adapter;
    private String value;

    public GetFavVideosAync(Context context) {
        this.context = context;
    }

    @Override
    protected Void doInBackground(String... params) {
        SQLiteDatabase db = new DatabaseHelper(this.context).getReadableDatabase();
        String query = "SELECT url FROM videos WHERE isFavorite = 1";
        Cursor cursor = null;
                cursor = db.rawQuery(String.format(query), null);
        value = cursor.toString();
        cursor.close();
        return null;

    }


    @Override
    protected void onPostExecute(Void arg) {
    this.video.favorite = true;
    if (!this.videoFilters.contains(VideoFilter.FAVORITE)) {
//        this.adapter.getVideos().remove(this.video);
        if(video.link.equals(value)) {
            this.adapter.getVideos().add(this.video);
        }
    }

    this.adapter.notifyDataSetChanged();
}
}
