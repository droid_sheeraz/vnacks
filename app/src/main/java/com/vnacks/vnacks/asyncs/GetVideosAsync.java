package com.vnacks.vnacks.asyncs;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.vnacks.vnacks.adapters.VideosRecyclerViewAdapter;
import com.vnacks.vnacks.database.DatabaseHelper;
import com.vnacks.vnacks.models.Video;
import com.vnacks.vnacks.models.VideoFilter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by seung on 6/6/2016.
 */
public class GetVideosAsync extends AsyncTask<String, Void, List<Video>> {

    private static final String TAG = GetVideosAsync.class.getSimpleName();

    public enum SortBy {
        TIME(0),
        LIKES(1);

        public final int value;

        private SortBy(int value) {
            this.value = value;
        }
    }

    public static final int BATCH_SIZE = 60;

    private Context context;
    private VideosRecyclerViewAdapter adapter;
    private EnumSet<VideoFilter> videoFilters;

    public GetVideosAsync(Context context, VideosRecyclerViewAdapter adapter, EnumSet<VideoFilter> filters) {
        this.context = context;
        this.adapter = adapter;
        this.videoFilters = filters;
    }

    @Override
    protected List<Video> doInBackground(String... params) {
        List<Video> videos = new ArrayList<Video>();
        /*
        SQLiteDatabase db = new DatabaseHelper(this.context).getReadableDatabase();

        Set<String> watchedVideos = new HashSet<String>();
        Cursor cursor = db.rawQuery(String.format("SELECT id FROM watched_videos"), null);
        while (cursor.moveToNext()) {
            String id = cursor.getString(0);
            watchedVideos.add(id);
        }
        cursor.close();
        */


//        Videos
//        https://www.facebook.com/ShortVideoClips/videos/712389335464104/https://www.facebook.com/ShortVideoClips/videos/712389335464104/
//
//        https://www.facebook.com/ShortVideoClips/videos/690644547638583/
//
//        https://www.facebook.com/ShortVideoClips/videos/713702155332822/
//
//        https://www.facebook.com/ShortVideoClips/videos/772335502802820/




        try {
            String query = +params.length > 0 ? params[0] : null;
            Log.i("Videos Query", query);

            URL url = new URL(query == null ? "http://115.68.230.134:3000/videos" : "http://115.68.230.134:3000/videos" + query);

//            Uri.parse(video.link.toString())
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            BufferedReader streamReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            StringBuilder responseStrBuilder = new StringBuilder();

            String inputStr;
            while ((inputStr = streamReader.readLine()) != null)
                responseStrBuilder.append(inputStr);

            Log.v(TAG, "Videos response: " + responseStrBuilder.toString());

            JSONArray campaignsJson = new JSONArray(responseStrBuilder.toString());
            for (int i=0; i< campaignsJson.length(); i++) {
                JSONObject videoJson = campaignsJson.getJSONObject(i);

                Video video = new Video();

                video.id = videoJson.getString("_id");

                Log.v(TAG, "id: " + video.id);

                video.sourceId = videoJson.getString("sourceId");
                video.fbId = videoJson.getLong("fbId");
                video.description = videoJson.getString("description");
                video.link = new URL(videoJson.getString("link"));
                video.picture = new URL(videoJson.getString("picture"));
                //video.createdTime = new Date(campaignJson.getString("createdTime"));
                video.duration = videoJson.getDouble("duration");
                video.likes = videoJson.getInt("likes");

                /*
                if (watchedVideos.contains(video.id)) {
                    video.watched = true;
                }

                if (video.watched && this.videoFilters.contains(VideoFilter.WATCHED)) {
                    videos.add(video);
                } else if (!video.watched && this.videoFilters.contains(VideoFilter.NOT_WATCHED)) {
                    videos.add(video);
                }
                */
                videos.add(video);
            }

        } catch (Exception e) {
            Log.e("", e.toString());
        }

        return videos;
    }

    @Override
    protected void onPostExecute(List<Video> videos) {
        this.adapter.getVideos().addAll(videos);
        this.adapter.notifyDataSetChanged();
    }

    public static String GetQuery(String collectionSuffix, SortBy sortBy, int index, int count) {
        return String.format("?suffix=%s&sortBy=%d&index=%d&count=%d", collectionSuffix, sortBy.value, index, count);
    }
}
