package com.vnacks.vnacks.asyncs;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.net.URL;

/**
 * Created by seung on 6/6/2016.
 */
public class GetImageAsync extends AsyncTask<URL, Void, Bitmap> {
    public ImageView imageView;

    public GetImageAsync(ImageView view) {
        this.imageView = view;
    }

    @Override
    protected Bitmap doInBackground(URL... urls) {
        try {
            return BitmapFactory.decodeStream(urls[0].openConnection().getInputStream());
        }
        catch (Exception e) {
            Log.e("", "", e);
        }

        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bmp) {
        this.imageView.setImageBitmap(bmp);
        this.imageView.setVisibility(View.VISIBLE);
    }
}
