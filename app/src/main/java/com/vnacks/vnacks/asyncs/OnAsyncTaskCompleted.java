package com.vnacks.vnacks.asyncs;

/**
 * Created by seung on 7/21/2016.
 */
public interface OnAsyncTaskCompleted {
    void onTaskCompleted(Object result);
}
