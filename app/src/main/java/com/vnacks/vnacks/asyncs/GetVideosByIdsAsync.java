package com.vnacks.vnacks.asyncs;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vnacks.vnacks.adapters.VideosRecyclerViewAdapter;
import com.vnacks.vnacks.database.DatabaseHelper;
import com.vnacks.vnacks.models.Video;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by seung on 6/6/2016.
 */
public class GetVideosByIdsAsync extends AsyncTask<String, Void, List<Video>> {

    private static final String TAG = GetVideosByIdsAsync.class.getSimpleName();

    private OnAsyncTaskCompleted callback;

    public GetVideosByIdsAsync(OnAsyncTaskCompleted callback) {
        this.callback = callback;
    }

    @Override
    protected List<Video> doInBackground(String... videoIds) {
        List<Video> videos = new ArrayList<>();


//        https://www.facebook.com/ShortVideoClips/videos/712389335464104/
//
//        // https://www.facebook.com/ShortVideoClips/videos/712389335464104/
//
//        https://www.facebook.com/ShortVideoClips/videos/690644547638583/
//
//        https://www.facebook.com/ShortVideoClips/videos/713702155332822/
//
//        https://www.facebook.com/ShortVideoClips/videos/772335502802820/



        try {
            URL url = new URL("http://115.68.230.134:3000/videos/byIds");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type","application/json");

            OutputStream out = conn.getOutputStream();
            String requestBody = new Gson().toJson(videoIds);
            out.write(requestBody.getBytes());
            out.flush();

            BufferedReader streamReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            StringBuilder responseStrBuilder = new StringBuilder();

            String responseLine;
            while ((responseLine = streamReader.readLine()) != null)
                responseStrBuilder.append(responseLine);

            String response = responseStrBuilder.toString();

            Log.v(TAG, "response: " + response);

            Gson gson = new Gson();
            Type listType = new TypeToken<ArrayList<Video>>(){}.getType();
            videos = gson.fromJson(response, listType);

        } catch (Exception e) {
            Log.e("", e.toString());
        }

        return videos;
    }

    @Override
    protected void onPostExecute(List<Video> videos) {
        this.callback.onTaskCompleted(videos);
    }
}
