package com.vnacks.vnacks.asyncs;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.vnacks.vnacks.database.DatabaseHelper;
import com.vnacks.vnacks.models.Video;
import com.vnacks.vnacks.models.VideoFilter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.HashMap;

/**
 * Created by laraib on 11/16/2017.
 */

public class SetFavVideosAsync extends AsyncTask<Void, Void, Void> {

    private Context context;
    private URL videoUrl;

    public SetFavVideosAsync(Context context, URL videoUrl) {
        this.context = context;
        this.videoUrl = videoUrl;
    }

    @Override
    protected Void doInBackground(Void... params) {
        SQLiteDatabase db = new DatabaseHelper(this.context).getWritableDatabase();
        ContentValues values = new ContentValues();
//        StringBuilder responseStrBuilder = new StringBuilder();
//        JSONArray campaignsJson = null;
//        JSONObject videoJson = null;
//        try {
//            campaignsJson = new JSONArray(responseStrBuilder.toString());
//            for (int i = 0; i < campaignsJson.length(); i++)
////                values.put("url", videoJson.getString("link"));
                values.put("url", String.valueOf(videoUrl));
            values.put("isFavorite", 1);
            db.insert("videos", null, values);
//        } catch (JSONException
//                e) {
//            e.printStackTrace();
//        }
        db.close();

        return null;
    }

    @Override
    protected void onPostExecute(Void arg) {
          new GetFavVideosAync(this.context);
    }

}
