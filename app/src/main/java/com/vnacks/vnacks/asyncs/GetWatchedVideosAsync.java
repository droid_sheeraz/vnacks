package com.vnacks.vnacks.asyncs;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.vnacks.vnacks.database.DatabaseHelper;
import com.vnacks.vnacks.models.Video;

import java.util.List;

/**
 * Created by laraib on 11/26/2017.
 */

public class GetWatchedVideosAsync extends AsyncTask<String, Void, List<Video>> {

    private Context context;
    private Video video;

    public GetWatchedVideosAsync(Context context, Video video) {
        this.context = context;
        this.video = video;
    }

    @Override
    protected List<Video> doInBackground(String... params) {
        SQLiteDatabase db = new DatabaseHelper(this.context).getReadableDatabase();
        Cursor cursor = db.rawQuery(String.format("SELECT url FROM videos WHERE isWatched = 1"), null);
        cursor.close();
        return null;
    }
}
