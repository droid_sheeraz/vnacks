package com.vnacks.vnacks.views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.gson.Gson;
import com.vnacks.vnacks.R;
import com.vnacks.vnacks.asyncs.GetVideosByIdsAsync;
import com.vnacks.vnacks.asyncs.OnAsyncTaskCompleted;
import com.vnacks.vnacks.asyncs.SetFavVideosAsync;
import com.vnacks.vnacks.models.Video;

import org.parceler.Parcels;

import java.util.List;

public class VideoActivity extends AppCompatActivity {

    private static final String TAG = VideoActivity.class.getSimpleName();

    private Video video;
    private VideoView videoView;
    private MediaController mediaController;
    private boolean showingMenu = true;

    public static final String EXTRA_VIDEO = "extra_video";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        Bundle bundle = getIntent().getExtras();
        String videoId = bundle.getString("VIDEO_ID");

        Log.v(TAG, "videoId: " + videoId);

        //new GetVideosByIdsAsync(this).execute(videoId);

        this.videoView = (VideoView)findViewById(R.id.videoView);
        this.videoView.setVisibility(View.INVISIBLE);


        this.mediaController = new MediaController(this) {
            @Override
            public void show(int timeout) {
                super.show(0);
            }
        };

        if(getIntent().getParcelableExtra(EXTRA_VIDEO) != null){

            this.video = Parcels.unwrap( getIntent().getParcelableExtra(EXTRA_VIDEO) );

            populateVideo();

        }

        this.mediaController.setPrevNextListeners(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //next button clicked
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //previous button clicked
            }
        });

    }

//    @Override
//    public void onTaskCompleted(Object result) {
//        List<Video> videos = (List<Video>)result;
//        this.video = videos.get(0);
//
//    }

    public void populateVideo(){
        Log.v(TAG, "video: " + new Gson().toJson(video) );

        TextView likesTextView = (TextView)this.findViewById(R.id.likes);
        likesTextView.setText(this.video.likes + "");

        TextView descriptionTextView = (TextView)this.findViewById(R.id.description);
        if (this.video.description == null || this.video.description == "") {
            descriptionTextView.setVisibility(View.GONE);
        }
        else {
            descriptionTextView.setText(this.video.description);
        }

        this.videoView.setVideoPath(this.video.link.toString()); //TODO : use hard coded for video testing

        if(video.strLink != null){
            this.videoView.setVideoPath(this.video.strLink);
        }

        this.videoView.start();
        this.videoView.setVisibility(View.VISIBLE);

        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent e) {
        if (e.getAction() == MotionEvent.ACTION_UP) {
            View videoMenu = findViewById(R.id.videoMenu);

            if (!showingMenu) {
                videoMenu.setVisibility(View.VISIBLE);
                mediaController.show(0);
            } else {
                videoMenu.setVisibility(View.INVISIBLE);
                mediaController.hide();
            }

            showingMenu = !showingMenu;
        }

        return super.dispatchTouchEvent(e);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            this.finish();
            return true;
        }

        return super.dispatchKeyEvent(event);
    }

    public void onClickFavoriteBtn(View v) {
        //new SetFavVideosAsync(getApplicationContext(), this.video.link);


        if(video != null) {
            video.favorite = true;
            video.strPicture = video.picture.toString();
            video.strLink = video.link.toString();
            video.save();
            Toast.makeText(this, "Added to the favorite list", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(this, "Could not save the video", Toast.LENGTH_SHORT).show();
        }


    }

    public void onClickShareBtn(View v) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, "Sharing video link");
        startActivity(Intent.createChooser(intent, "Share"));
        //Toast.makeText(this, "onClickShareBtn", Toast.LENGTH_SHORT).show();
    }

    public void onClickLikeBtn(View v) {
        int fav = 0;
        fav++;
        Toast.makeText(this, "Added to the favorite list", Toast.LENGTH_SHORT).show();
    }
}
