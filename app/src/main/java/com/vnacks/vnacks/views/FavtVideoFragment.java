package com.vnacks.vnacks.views;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.raizlabs.android.dbflow.sql.language.Select;
import com.vnacks.vnacks.R;
import com.vnacks.vnacks.adapters.VideosRecyclerViewAdapter;
import com.vnacks.vnacks.models.Video;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sheerazam on 01/12/2017.
 */

public class FavtVideoFragment extends Fragment implements VideosFragment.OnListFragmentInteractionListener {

    public static final String TAG = FavtVideoFragment.class.getSimpleName();

    VideosRecyclerViewAdapter adapter;


    RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_favt, container, false);
        adapter = new VideosRecyclerViewAdapter(new ArrayList<Video>(), this);

        recyclerView = rootView.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.v(TAG, "onResume()");
        List<Video> listVideos = new Select().from(Video.class).queryList();
        adapter.setData(listVideos);
    }

    @Override
    public void onListFragmentInteraction(Video video) {
        Uri uri = Uri.parse(video.strLink);
        Intent intent = new Intent(this.getContext(), VideoActivity.class);
        intent.putExtra("VIDEO_ID", video.id);
        intent.putExtra( VideoActivity.EXTRA_VIDEO, Parcels.wrap(video) );
        startActivity(intent);
    }
}
