package com.vnacks.vnacks.views;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.sql.language.Select;
import com.vnacks.vnacks.R;
import com.vnacks.vnacks.adapters.VideosRecyclerViewAdapter;
import com.vnacks.vnacks.asyncs.GetVideosAsync;
import com.vnacks.vnacks.models.Video;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by laraib on 1/9/2018.
 */

public class WatchedVideoFragment extends Fragment {
    public static final String TAG = WatchedVideoFragment.class.getSimpleName();
    private VideosRecyclerViewAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private VideosFragment.OnListFragmentInteractionListener mListener;
    private boolean isLoading;
    private int previousTotalCount = GetVideosAsync.BATCH_SIZE;
    private int totalItemCount;
    private int visibleItemCount;
    private int pastVisibleItems;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
//        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_video_list, container, false);

        // Set the adapter
        Context context = view.getContext();
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list);

        this.mAdapter = new VideosRecyclerViewAdapter(new ArrayList<Video>(), mListener);
        this.mLayoutManager = new LinearLayoutManager(context);

        recyclerView.setAdapter(this.mAdapter);
        recyclerView.setLayoutManager(this.mLayoutManager);

//        new GetVideosAsync(this.getContext(), this.mAdapter, this.videoFilters).execute(this.getQueryByType(0));
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {



            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    totalItemCount = mLayoutManager.getItemCount();
                    visibleItemCount = mLayoutManager.getChildCount();
                    pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();
                }
            }
        });
        return view;
//        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        List<Video> listVideos = new Select().from(Video.class).queryList();
        for(Video video:listVideos)
        {
            Log.v(TAG, "video description" + video.description);
        }
    }
}
