package com.vnacks.vnacks.views;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vnacks.vnacks.R;
import com.vnacks.vnacks.adapters.VideosRecyclerViewAdapter;
import com.vnacks.vnacks.asyncs.GetVideosAsync;
import com.vnacks.vnacks.asyncs.SetWatchedVideoAsync;
import com.vnacks.vnacks.models.Video;
import com.vnacks.vnacks.models.VideoFilter;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */

public class VideosFragment extends Fragment {

    public enum Type {
        NEW(0),
        BEST(1),
        WATCHED(2),
        FAVORITES(3);

        public int value;

        private Type(int value) {
            this.value = value;
        }
    }

    private String suffix = "";
    private boolean isLoading;
    private int previousTotalCount = GetVideosAsync.BATCH_SIZE;
    private Type type;
    private EnumSet<VideoFilter> videoFilters;
    private LinearLayoutManager mLayoutManager;
    private VideosRecyclerViewAdapter mAdapter;
    private OnListFragmentInteractionListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public VideosFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static VideosFragment newInstance(Type type) {
        VideosFragment fragment = new VideosFragment();

        Bundle args = new Bundle();
        args.putInt("Type", type.value);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.type = Type.values()[this.getArguments().getInt("Type")];
        switch (this.type) {
            case NEW:
                this.videoFilters = EnumSet.of(VideoFilter.NOT_WATCHED, VideoFilter.WATCHED);
                break;
            case BEST:
                this.videoFilters = EnumSet.of(VideoFilter.NOT_WATCHED, VideoFilter.WATCHED);
                break;
            case WATCHED:
                this.videoFilters = EnumSet.of(VideoFilter.WATCHED);
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_list, container, false);

        // Set the adapter
        Context context = view.getContext();
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list);

        this.mAdapter = new VideosRecyclerViewAdapter(new ArrayList<Video>(), mListener);
        this.mLayoutManager = new LinearLayoutManager(context);

        recyclerView.setAdapter(this.mAdapter);
        recyclerView.setLayoutManager(this.mLayoutManager);

        new GetVideosAsync(this.getContext(), this.mAdapter, this.videoFilters).execute(this.getQueryByType(0));
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            private int totalItemCount;
            private int visibleItemCount;
            private int pastVisibleItems;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    totalItemCount = mLayoutManager.getItemCount();
                    visibleItemCount = mLayoutManager.getChildCount();
                    pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (isLoading && totalItemCount > previousTotalCount) {
                        isLoading = false;
                         previousTotalCount = totalItemCount;
                    }

                    if (!isLoading && (visibleItemCount +  pastVisibleItems + 20) >= totalItemCount) {
                        isLoading = true;
                        new GetVideosAsync(getContext(), mAdapter, videoFilters).execute(getQueryByType(totalItemCount));
                    }
                }
            }
        });
        return view;
    }

    private String getQueryByType(int index) {
        switch (this.type) {
            case NEW:
            case WATCHED:
                return GetVideosAsync.GetQuery(this.suffix, GetVideosAsync.SortBy.TIME, index, GetVideosAsync.BATCH_SIZE);
            case BEST:
                return GetVideosAsync.GetQuery(this.suffix, GetVideosAsync.SortBy.LIKES, index, GetVideosAsync.BATCH_SIZE);
            default:
                return "";
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void onClickVideo(Video video) {
        Uri uri = Uri.parse(video.link.toString());
        Intent intent = new Intent(this.getContext(), VideoActivity.class);
        intent.putExtra("VIDEO_ID", video.id);
        intent.putExtra( VideoActivity.EXTRA_VIDEO, Parcels.wrap(video) );
        startActivity(intent);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Video item);
    }
}
