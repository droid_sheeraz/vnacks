package com.vnacks.vnacks.views;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.vnacks.vnacks.R;
import com.vnacks.vnacks.adapters.TabsAdapter;
import com.vnacks.vnacks.models.Video;

public class MainActivity extends AppCompatActivity implements VideosFragment.OnListFragmentInteractionListener, SettingsFragment.OnFragmentInteractionListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */

    private TabsAdapter tabsAdapter;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        this.tabsAdapter = new TabsAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        this.viewPager = (ViewPager) findViewById(R.id.container);
            if (this.viewPager == null) {
            return;
        }

        this.viewPager.setAdapter(this.tabsAdapter);
        viewPager.setOffscreenPageLimit(5);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        if (tabLayout == null) {
            return;
        }

        tabLayout.setupWithViewPager(this.viewPager);
        for (int i=0; i< tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab == null) {
                return;
            }

            //tab.setIcon(R.mipmap.ic_launcher);
        }

    }

    @Override
    public void onListFragmentInteraction(Video video) {
        int position = this.viewPager.getCurrentItem();
        ((VideosFragment) this.tabsAdapter.getItem(position)).onClickVideo(video);
        if(video != null) {
            video.watched = true;
            video.save();
            Toast.makeText(this, "Added to the watched video list", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(this, "Could not save the video", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        
    }
}
