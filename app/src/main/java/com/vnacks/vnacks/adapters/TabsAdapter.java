package com.vnacks.vnacks.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.vnacks.vnacks.views.FavtVideoFragment;
import com.vnacks.vnacks.views.SettingsFragment;
import com.vnacks.vnacks.views.VideosFragment;
import com.vnacks.vnacks.views.WatchedVideoFragment;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class TabsAdapter extends FragmentPagerAdapter {

    public VideosFragment newVideosFragment;
    public VideosFragment bestVideosFragment;
    public WatchedVideoFragment watchedVideosFragment;
    public FavtVideoFragment favoriteVideosFragment;
    public SettingsFragment settingsFragment;

    public TabsAdapter(FragmentManager fm) {
        super(fm);
        this.newVideosFragment = VideosFragment.newInstance(VideosFragment.Type.NEW);
        this.bestVideosFragment = VideosFragment.newInstance(VideosFragment.Type.BEST);
//        this.watchedVideosFragment = VideosFragment.newInstance(VideosFragment.Type.WATCHED);
        this.watchedVideosFragment = new WatchedVideoFragment();
        this.favoriteVideosFragment = new FavtVideoFragment();
        this.settingsFragment = SettingsFragment.newInstance(null, null);
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        switch (position) {
            case 0:
            default:
                return this.newVideosFragment;
            case 1:
                return this.bestVideosFragment;
            case 2:
                return this.watchedVideosFragment;
            case 3:
                return this.favoriteVideosFragment;
            case 4:
                return this.settingsFragment;
        }
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 5;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "New";
            case 1:
                return "Best";
            case 2:
                return "Watched";
            case 3:
                return "Favorites";
            case 4:
                return "Settings";
        }
        return null;
    }
}