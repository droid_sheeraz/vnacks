package com.vnacks.vnacks.adapters;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.vnacks.vnacks.R;
import com.vnacks.vnacks.views.VideosFragment.OnListFragmentInteractionListener;
import com.vnacks.vnacks.asyncs.GetImageAsync;
import com.vnacks.vnacks.models.Video;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Video} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class VideosRecyclerViewAdapter extends RecyclerView.Adapter<VideosRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = VideosRecyclerViewAdapter.class.getSimpleName();

    private List<Video> mValues;
    private final OnListFragmentInteractionListener mListener;

    public VideosRecyclerViewAdapter(List<Video> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_video, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.likesView.setText(String.format("%s", holder.mItem.likes));
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        holder.durationView.setText(decimalFormat.format(holder.mItem.duration) + "s");

        String description = mValues.get(position).description;
        if (description == null || description.length() == 0 || description.equals("null")) {
            holder.descriptionView.setVisibility(View.GONE);
        }
        else {
            holder.descriptionView.setText(description);
        }

        //holder.pictureView.setVisibility(View.INVISIBLE);

        String imageUrlString = "";

        if(mValues.get(position).picture != null) {

            URL imageUrl = mValues.get(position).picture;
            Log.v(TAG, "imageUrl: " + imageUrl.toString());
            imageUrlString = imageUrl.toString();

        }

        if (mValues.get(position).strPicture != null){

            imageUrlString = mValues.get(position).strPicture;

        }

        Uri uri = Uri.parse(imageUrlString);

        holder.pictureView.setImageURI(uri);

        //new GetImageAsync(holder.pictureView).execute(mValues.get(position).picture);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public List<Video> getVideos() {
        return this.mValues;
    }

    public void setData(List<Video> listVideos){
        mValues = listVideos;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView descriptionView;
        public final TextView likesView;
        public final TextView durationView;

        public final SimpleDraweeView pictureView;
        public Video mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            this.descriptionView = (TextView) view.findViewById(R.id.description);
            this.likesView = (TextView) view.findViewById(R.id.likes);
            this.durationView = (TextView) view.findViewById(R.id.duration);
            this.pictureView = (SimpleDraweeView) view.findViewById(R.id.picture);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + descriptionView.getText() + "'";
        }
    }
}
