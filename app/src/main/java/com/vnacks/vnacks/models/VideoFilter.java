package com.vnacks.vnacks.models;

/**
 * Created by seung on 6/16/2016.
*/

public enum VideoFilter {
    WATCHED,
    NOT_WATCHED,
    FAVORITE,
    SAVED,
    NOT_SAVED
}