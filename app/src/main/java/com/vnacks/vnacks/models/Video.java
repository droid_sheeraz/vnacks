package com.vnacks.vnacks.models;

/**
 * Created by seung on 6/6/2016.
 */

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;
import com.vnacks.vnacks.database.MyDatabase;

import org.parceler.Parcel;

import java.net.URL;
import java.util.Date;

@Table(database = MyDatabase.class)
@Parcel(analyze = Video.class)
public class Video extends BaseModel {

    @Column
    @PrimaryKey
    @SerializedName("_id")
    public String id;

    @Column
    @SerializedName("sourceId")
    public String sourceId;

    @Column
    @SerializedName("fbId")
    public long fbId;

    @Column
    @SerializedName("description")
    public String description;


    public URL link;

    public URL picture;

    @Column
    public String strPicture;

    @Column
    public String strLink;

    public Date createdTime;

    public double duration;

    public int likes;

    @Column
    public boolean watched;

    @Column
    public boolean favorite;

    public Video() {
        this.id = "57540dab71f779d17eeed839";
        this.sourceId = "57515028c333cb9f1794848e";
        this.fbId = 1150278668372903L;
        this.description = "When you reach your limit with that co-worker.";
        this.createdTime = new Date();
        this.duration = 3.067;
        this.likes = 93;

        try {
            this.link = new URL("https://video.xx.fbcdn.net/v/t42.1790-2/13381890_1628571307464079_1140203937_n.mp4?efg=eyJybHIiOjU1NiwicmxhIjo1MTIsInZlbmNvZGVfdGFnIjoic3ZlX3NkIn0%3D&rl=556&vabr=309&oh=45a663c6ddbf00ce7ca93a80ab3b1b2e&oe=57561F54");
            this.picture = new URL("https://scontent.xx.fbcdn.net/v/t15.0-10/p128x128/12719737_1150278951706208_1624497157_n.jpg?oh=f2d192928051e1f6cfc87d2b2b183a25&oe=57DE6DD4");
        }
        catch (Exception e) {

        }
    }
}
