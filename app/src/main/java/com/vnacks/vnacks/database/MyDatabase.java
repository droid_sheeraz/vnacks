package com.vnacks.vnacks.database;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by sheerazam on 30/11/2017.
 */
@Database(name = MyDatabase.NAME, version = MyDatabase.VERSION)
public class MyDatabase {

    public static final String NAME = "MyDatabase";
    public static  final int VERSION = 1;

}
