package com.vnacks.vnacks.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by seung on 6/15/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {


    public DatabaseHelper(Context context) {
        super(context, "VNackss.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE watched_videos (id TEXT PRIMARY KEY, time DATETIME)");
        db.execSQL("CREATE TABLE videos (id TEXT PRIMARY KEY, time DATETIME, url TEXT, isWatched int, isFavorite int)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS watched_videos");
        db.execSQL("DROP TABLE IF EXISTS videos");
        this.onCreate(db);
    }
}
