package com.vnacks.vnacks;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.stetho.Stetho;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

/**
 * Created by sheerazam on 30/11/2017.
 */

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        //dbflow - database
        FlowManager.init(new FlowConfig.Builder(this).build());

        //stetho
        Stetho.initializeWithDefaults(this);

        //fresco - image downloading and caching
        Fresco.initialize(this);
    }
}
